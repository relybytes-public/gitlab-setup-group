# Gitlab Setup Group

This project allow the user to initialize a group on gitlab to allow the manage for scrupt or more complex scenario.
It is indicated to use for software house company or company where follow the developement process in a Web/Firmware sections.

# Summary

1. [Architecture](#architecture)
2. [Repository](#repository)
3. [Installation](#installation)
4. [Build](#build)
5. [Run](#run)
6. [Deploy](#deploy)
7. [Test](#test)
8. [Documentation](#documentation)

# Architecture

This project use python and must be run only one time, the only input is a configuration file with the groupid and the url of the gitlab istance.

# Repository

* setup.py -> logic and executable file

# Installation

The requirements for the installation are:
* python `3.10.X`
* The gitlab istance (cloud or selfhosted)
* An access token with "API" grant
* python gitlab library: `pip install --upgrade python-gitlab`

Create a gl.cfg file something like this:

```
[global]
default = gitlabserver
ssl_verify = true
timeout = 5
per_page = 50

[gitlabserver]
url = https://gitlab.com
private_token = <your token>
api_version = 4

[applicationsettings]
group_id = <your groupid>
```

# Build

No build necessary

# Run

`python setup.py`

# Deploy

No deploy needed

# Test

The test suite is not present

# Documentation

Only this file
Library used: https://python-gitlab.readthedocs.io/en/stable/api-usage.html
