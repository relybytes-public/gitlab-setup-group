import gitlab
import configparser

def confirm():
    """
    Ask user to enter Y or N (case-insensitive).
    :return: True if the answer is Y.
    :rtype: bool
    """
    answer = ""
    while answer not in ["y", "n"]:
        answer = input("OK to push to continue [Y/N]? ").lower()
    return answer == "y"

def foundlabel_add_list(board, labels_created, label_name):
    label_found = next((x for x in labels_created if x.name == label_name), None)
    board.lists.create({'label_id': label_found.id})

path_to_config = 'gl.cfg'
config = configparser.RawConfigParser()
config.read(path_to_config)

group_id = dict(config.items('applicationsettings'))['group_id']

# create gitlab istance
gl = gitlab.Gitlab.from_config('gitlabserver', [path_to_config])

group = gl.groups.get(group_id)

print('Foud this group: ' + str(group.name) + ' [' + str(group.get_id()) + '], if continue this create labels, groups and project template under this main group:')

confirm()

labels = group.labels.list()
boards = group.boards.list()

labels_to_create = [
    # Items structure: {'name': '', 'color': '', 'description': ''}
    # Type labels 
    {'name': 'BugFix', 'color': '#9f1414', 'description': 'Fix request due software bug reported from customer or from internal review'},
    {'name': 'New Feature', 'color': '#944c00', 'description': 'New request or new sub-functionality to a present functionality'},
    {'name': 'Change Request', 'color': '#a98600', 'description': 'Software change requests by customer or by internal review'},
    {'name': 'Annotation', 'color': '#c7dcc7', 'description': 'This is a particular type of label that is not part of a development but is a proposal for improvement / criticality to be discussed in a regular team meeting'},
    {'name': 'Maintenance', 'color': '#6d6d15', 'description': 'Maintenance activities, data analysis, data cleaning, improvements, remove procedure deprecated or batch deprecated'},
    # Status labels  (theorycally is better have scoped labels: state::)
    ## Complete management (with various phases)
    {'name': 'Ready to analysis', 'color': '#b3dbf0', 'description': 'Wait to analysis'},
    {'name': 'Analysis', 'color': '#0488d0', 'description': 'This phase is to review the customer / business request and specify individual requirements, define how to create the functionality, what file to modify, what check, what technical problems can be encountered and how to verify the functionality (Functional, Technical and Test revision)'},
    {'name': 'Ready to development', 'color': '#bfbac4', 'description': 'Status waiting to be taken over by the developers'},
    {'name': 'Development', 'color': '#59525f', 'description': 'Development phase'},
    {'name': 'Ready to validating', 'color': '#b3b6dc', 'description': 'Status waiting to be taken over by the testers'},
    {'name': 'Validating', 'color': '#434aa8', 'description': 'validation phase'},
    {'name': 'Ready to release', 'color': '#294800', 'description': 'Status waiting to be released. In this status the issue is closed (to make work the burndown chart) and on the release phase every task is checked and removed the label (and closed)'},
    ## Simple management / Scrum (with various phases)
    {'name': 'To Do', 'color': '#97bd60', 'description': 'State that indicate is in list to do. This state can be used for any tasks that are not in progress.'},
    {'name': 'Doing', 'color': '#3cb371', 'description': 'State that indicate is in progress. This state can be used for any tasks that are in progress and currently working on it. **It is very important to understand for how many things each person is working on**'},
    {'name': 'Done', 'color': '#22482f', 'description': 'Activity completed and validated. You can proceed to release it and then close it. All the tasks related to the issue are done, waiting to close when released on production.'},

    # Info labels
    ## Priority labels (theorycally is better have scoped labels: priority::low|normal..)
    {'name': 'Priority low', 'color': '#a48292', 'description': ''},
    {'name': 'Priority normal', 'color': '#5c1d3b', 'description': ''},
    {'name': 'Priority high', 'color': '#4a0526', 'description': ''},
    {'name': 'Priority immediate', 'color': '#1d020f', 'description': 'Hotfix'},
    
    {'name': 'Need information', 'color': '#696969', 'description': ''},
    {'name': 'Need discussion', 'color': '#0000ff', 'description': ''},
    {'name': 'Docs', 'color': '#fafad2', 'description': ''},
    {'name': 'Requirements', 'color': '#e4ede0', 'description': ''},
    ]

labels_created = []

for label in labels_to_create:
    label_found = next((x for x in labels if x.name == label["name"]), None)
    if label_found is None :
        label_created = group.labels.create(label)
        print('Created label: ' + str(label))
        labels_created.append(label_created)
    else :
        print('Just present label: ' + str(label))
        labels_created.append(label_found)

board_scrum = ['To Do', 'Doing', 'Done']
board_complete = ['Ready to analysis', 'Analysis', 'Ready to development', 'Development', 'Ready to validating', 'Validating', 'Ready to release']

if len(boards) >= 1:
    board = boards[0]
    print(str(board.get_id()))
    b_lists = board.lists.list()
    for list_to_delete in b_lists:
        list_to_delete.delete()

    print('Want to create a complete (Y) or scrum board (N)?')
    value = confirm()
    boards_lists = board_complete if value else board_scrum
    for label_cicle in boards_lists:
        foundlabel_add_list(board, labels_created, label_cicle)

print('Want to create a sub-group called Clients?')

value = confirm()
if value == True:
    subgroups = group.subgroups.list()
    subgroup_client = next((x for x in subgroups if x.path == 'clients'), None)
    if subgroup_client is None:
        gl.groups.create({'name': 'Clients', 'path': 'clients', 'parent_id': group.get_id()})
    else: 
        print('Do nothing, just exist')
else: 
    print('Do nothing')






